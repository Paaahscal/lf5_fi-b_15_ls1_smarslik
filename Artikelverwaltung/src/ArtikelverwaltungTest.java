
public class ArtikelverwaltungTest {

	public static void main(String[] args) {
		Artikelverwaltung art1 = new Artikelverwaltung();
		Artikelverwaltung art2 = new Artikelverwaltung();
		Artikelverwaltung art3 = new Artikelverwaltung();
		Artikelverwaltung art4 = new Artikelverwaltung("Corona");
		Artikelverwaltung art5 = new Artikelverwaltung("Heineken",0005,40,120,0.78,1.29);
		

		
		//Setzen der Attribute
		art1.setBezeichnung("Dosenbier");
		art1.setArtikelnummer(0001);
		art1.setLagerbestand(120);
		art1.setMindestbestand(100);
		art1.setEinkaufspreis(0.34);
		art1.setVerkaufspreis(0.69);

		art2.setBezeichnung("Flaschenbier");
		art2.setArtikelnummer(0002);
		art2.setLagerbestand(40);
		art2.setMindestbestand(100);
		art2.setEinkaufspreis(0.45);
		art2.setVerkaufspreis(0.79);
		
		art3.setBezeichnung("Fassbier");
		art3.setArtikelnummer(0003);
		art3.setLagerbestand(30);
		art3.setMindestbestand(20);
		art3.setEinkaufspreis(2.63);
		art3.setVerkaufspreis(8.69);
		
		System.out.println("Artikelname: " + art1.getBezeichnung());
		System.out.println(art1.fillStorage());
		System.out.println(art2.fillStorage());
		System.out.println(art3.fillStorage());
		System.out.println(art4.fillStorage());
		System.out.println(art5.fillStorage());
	}

}
