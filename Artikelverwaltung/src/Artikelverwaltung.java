
public class Artikelverwaltung {
	// Attribute
	private int Artikelnummer, Mindestbestand, Lagerbestand;
	private double Einkaufspreis, Verkaufspreis;
	private String Bezeichnung;

	public Artikelverwaltung() {

	}

	public Artikelverwaltung(String Bezeichnung) {
		this.Bezeichnung = Bezeichnung;
	}

	public Artikelverwaltung(String Bezeichnung, int Artikelnummer, int Lagerbestand, int Mindestbestand,
			double Einkaufspreis, double Verkaufspreis) {
		this.Bezeichnung = Bezeichnung;
		this.Artikelnummer = Artikelnummer;
		this.Lagerbestand = Lagerbestand;
		this.Mindestbestand = Mindestbestand;
		this.Einkaufspreis = Einkaufspreis;
		this.Verkaufspreis = Verkaufspreis;
	}

	public int getMindestbestand() {
		return Mindestbestand;
	}

	public void setMindestbestand(int mindestbestand) {
		Mindestbestand = mindestbestand;
	}

	public int getLagerbestand() {
		return Lagerbestand;
	}

	public void setLagerbestand(int lagerbestand) {
		Lagerbestand = lagerbestand;
	}

	public int getArtikelnummer() {
		return Artikelnummer;
	}

	public void setArtikelnummer(int artikelnummer) {
		Artikelnummer = artikelnummer;
	}

	public double getEinkaufspreis() {
		return Einkaufspreis;
	}

	public void setEinkaufspreis(double einkaufspreis) {
		Einkaufspreis = einkaufspreis;
	}

	public double getVerkaufspreis() {
		return Verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		Verkaufspreis = verkaufspreis;
	}

	public String getBezeichnung() {
		return Bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		Bezeichnung = bezeichnung;
	}

	// Methoden

	public void order() {

	}

	public double calculateProfit() {
		double profit = Verkaufspreis - Einkaufspreis;
		return profit;
	}

	public void changeLagerbestand(int lagerbestand) {
		this.Lagerbestand = lagerbestand;
	}

	public String fillStorage() {
		String Ausgabe;
		if (Lagerbestand <= Mindestbestand * 0.8) {
			int Bestellung = (Mindestbestand - Lagerbestand) * 2;
			Ausgabe = "Ich habe " + Bestellung + " " + Bezeichnung + " nachbestellt.";
			double price = Bestellung * Einkaufspreis;
			Ausgabe = Ausgabe + "\nDas kostet uns " + price + " Euro.";
		} else {
			Ausgabe = "Wir m�ssen nichts nachbestellen.";
		}
		return Ausgabe;
	}
}
