import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		int fahrzeit = 0;
		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau
		System.out.println("M�chtest du in Spandau halten?");
		char haltInSpandau = console.next().charAt(0);
		if (haltInSpandau == 'j') {
			fahrzeit += 2; // Halt in Spandau
		}
		System.out.println("Soll es nach Hamburg gehen?");
		char richtungHamburg = console.next().charAt(0);
		if (richtungHamburg == 'j') {
			fahrzeit += 96;
			angekommen(fahrzeit, 'h', richtungHamburg);
		} else {
			System.out.println("Soll der Zug in Stendal halten?");
			char haltInStendal = console.next().charAt(0);
			System.out.println("Wo soll der Zug halten?");
			char endetIn = console.next().charAt(0);
			fahrzeit += 34;
			if (haltInStendal == 'j') {
				fahrzeit += 16;
			} else {
				fahrzeit += 6;
			}
			if (endetIn == 'w') {
				fahrzeit += 29;
				angekommen(fahrzeit, endetIn, richtungHamburg);
			} else if (endetIn == 'b') {
				fahrzeit += 50;
				angekommen(fahrzeit, endetIn, richtungHamburg);
			} else {
				fahrzeit += 62;
				angekommen(fahrzeit, endetIn, richtungHamburg);
			}
		}
	}

	public static void angekommen(int fahrzeit, char endetIn, char richtungHamburg) {
		String stadt = "";
		if (richtungHamburg == 'j') {
			stadt = "Hamburg";
		} else {
			if (endetIn == 'w') {
				stadt = "Wolfsburg";
			} else if (endetIn == 'b') {
				stadt = "Braunschweig";
			} else if (endetIn == 'h') {
				stadt = "Hannover";
			}
		}
		System.out.printf("Sie erreichen %s nach %d Minuten.", stadt, fahrzeit);
	}

}
