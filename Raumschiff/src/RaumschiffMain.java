
public class RaumschiffMain {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2,"IKS Hegh�ta");
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
		klingonen.addLadung(new Ladung("Bat�leth Klingonen Schwert",200));
		
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
		romulaner.addLadung(new Ladung("Borg-Schrott",5));
		romulaner.addLadung(new Ladung("RoteMaterie",2));		
		romulaner.addLadung(new Ladung("Plasma-Waffe",50));
		
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni�Var");
		vulkanier.addLadung(new Ladung("Forschungssonde",35));
		vulkanier.addLadung(new Ladung("Photonentorpedo",3));
		
		romulaner.zustandRaumschiff();
		//romulaner.ladungsverzeichnis;
		romulaner.photonentorpedoSchiessen(vulkanier);
		romulaner.phaserkanoneSchiessen(vulkanier);
		romulaner.nachrichtAnAlle("WIR WERDEN EUCH VERNICHTEN");
		//romulaner.eintraegeLogbuchZurueckgeben();
		//romulaner.photonentorpedosLaden();
		//romulaner.ladungsverzeichnisAufraemen();
		romulaner.reperaturDurchfuehren(true,true,true,2);
		}

}
