import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	public Raumschiff() {
		broadcastKommunikator = new ArrayList<String>();
		ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		broadcastKommunikator = new ArrayList<String>();
		ladungsverzeichnis = new ArrayList<Ladung>();
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}

	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	public void ladungsverzeichnisAufraemen() {

	}

	public void photonentorpedoSchiessen(Raumschiff r) {
		// check ob es das eigene Raumschiff ist

		if (this.photonentorpedoAnzahl <= 0) {
			System.out.println(" -=*Click*=-");
		} else

		{
			this.photonentorpedoAnzahl--;
			System.out.println("Photonentorpedo abgeschossen");
		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent >= 50) {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			System.out.println("Phaserkanone abgeschossen");
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}

	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
		r.schildeInProzent = r.schildeInProzent - 50;
		if (r.schildeInProzent <= 0) {
			r.huelleInProzent = r.huelleInProzent - 50;
			r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
		}
		if (r.huelleInProzent <= 0) {
			r.lebenserhaltungssystemInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme von" + r.getSchiffsname() + "sind vernichtet worden.");
		}
	}

	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
	}

	/*
	 * public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
	 * 
	 * }
	 */

	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		Random zufall = new Random();
		int zufallsZahl = zufall.nextInt(100);
		int anzahlSchiffsstrukturen = 0;
		if (schutzschilde == true)
			anzahlSchiffsstrukturen++;
		if (energieversorgung == true)
			anzahlSchiffsstrukturen++;
		if (schiffshuelle == true)
			anzahlSchiffsstrukturen++;
		int prozReperatur = (zufallsZahl * anzahlDroiden) / anzahlSchiffsstrukturen;
		if (schutzschilde == true)
			this.schildeInProzent = this.schildeInProzent + prozReperatur;
		if (energieversorgung == true)
			this.energieversorgungInProzent = this.energieversorgungInProzent + prozReperatur;
		if (schiffshuelle == true)
			this.huelleInProzent = this.huelleInProzent + prozReperatur;
	}

	public void zustandRaumschiff() {
		System.out.println("Der Zustand vom Schiff: " + getSchiffsname());
		System.out.println("PhotonentorpedoAnzahl: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + getSchildeInProzent());
		System.out.println("H�lle in Prozent: " + getHuelleInProzent());
		System.out.println("Lebenserhaltungssystem in Prozent: " + getLebenserhaltungssystemInProzent());
		System.out.println("Anzahl Androiden: " + getAndroidenAnzahl());
	}
}
