
/* Author: Pascal Smarslik
 * Klasse: FI-B-15
 * Date: 08.12.2021
 * Version: v2
*/
import java.util.Scanner;

class FahrkartenautomatMethoden {
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen(); // erstell den Wert zuZahlenderBetrag und ruft die
																	// Methode fahrkartenbestellungErfassen auf
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag); // erstell den Wert
																					// eingezahlterGesamtbetrag und ruft
																					// die Methode
																					// fahrkartenbestellungBezahlen auf
																					// und �bergibt den
																					// zuZahlenderBetrag Parameter
		fahrkartenAusgeben(); // Die Fahrkarte wird ausgegeben, leider noch ohne �berpr�fung, ob wirklich das
								// eingezahlte Geld aussreicht, offiziell kenne ich den if Befehl noch nicht
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag); // das r�ckgeld wird ausgegeben und die
																		// Fahrkartenbestellung war erfolgreich
	}

	public static double fahrkartenbestellungErfassen() {
		double zuZahlenderBetrag;
		int Anzahl;
		System.out.print("Ticketpreis (Euro): ");// Fragt nach dem Ticketpreis
		zuZahlenderBetrag = tastatur.nextDouble();// Ticketpreis wird eingegen
		if (zuZahlenderBetrag <= 0) {// Testet, ob der Ticketpreis unter oder Null ist und informiert den K�ufer
			System.out.println(
					"\nLeider konnte ich ihre Eingabe nicht erfassen,\n" + "ihr Ticketpreis wird auf 1 gesetzt.");
			zuZahlenderBetrag = 1;// Und der Ticketpreis wird auf 1 gesetzt
		}
		System.out.print("\nAnzahl der Tickets (max. 10): ");// Wie viele Tickets will man haben
		Anzahl = tastatur.nextInt();// Anzahl wird eingegeben
		if (Anzahl > 10 || Anzahl < 1) { // Testet, ob die angegebenen Ticketanzahl nicht min. 1 max 10 ist, dann wird
											// der K�ufer informiert
			System.out.println(
					"\nLeider konnte ich ihre Eingabe nicht erfassen,\n" + "ihre Ticketanzahl wird auf 1 gesetzt.");
			Anzahl = 1;// Und die Ticketanzahl wird auf 1 gesetzt
		}
		zuZahlenderBetrag = zuZahlenderBetrag * Anzahl;// Tats�chlicher Preis wird berechnet
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("\nNoch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5 Ct, h�chstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("%s%.2f%s\n", "Der R�ckgabebetrag in H�he von ", rueckgabebetrag, " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}