import java.util.Scanner;
//Pascal Smarslik
public class AbSchleifen1for {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Welche Teilaufgabe soll ihc ausführen? (a,b,c,d oder e eingeben)");
		char programm = myScanner.next().charAt(0); // Eingabe von dem auszuführenden Progamm
		switch (programm) {
		case 'a':
			aufgabeA();
			break;
		case 'b':
			aufgabeB();
			break;
		case 'c':
			aufgabeC();
			break;
		case 'd':
			aufgabeD();
			break;
		case 'e':
			aufgabeE();
			break;
		default:
			System.out.println("Fehler!");
			break;
		}

	}

	public static void aufgabeA() {
		for (int i = 99; i >= 9; i = i - 3) {
			System.out.print(i);
			if (i>9)
				System.out.print(", ");
		}
	}

	public static void aufgabeB() {
		for (int i = 1; i <= 20; i++) {
			System.out.print(i * i);
			if(i<20)
				System.out.print(", ");
		}
	}

	private static void aufgabeC() {
		for (int i = 2; i <= 102; i = i + 4) {
			System.out.print(i);
			if(i<102)
				System.out.print(", ");
		}
	}

	private static void aufgabeD() {
		int zahl = 4;
		for (int i = 3; i <= 31; i = i + 2) {
			zahl = zahl + 4 * i;
			System.out.print(zahl);
			if(i<31)
				System.out.print(", ");
		}
	}

	private static void aufgabeE() {
		int zahl = 1;
		for (int i = 1; i <= 15; i++) {
				zahl = zahl * 2;
			
			System.out.print(zahl);
			if (i<15)
				System.out.print(", ");
		}
	}
}
