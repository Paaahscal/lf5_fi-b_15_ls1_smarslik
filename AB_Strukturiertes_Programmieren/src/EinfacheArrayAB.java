import java.util.Scanner;

public class EinfacheArrayAB {
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Welche Aufgabe soll ich ausf�hren? ( 1,2,3 oder 4 eingeben) ");
		int programm = myScanner.nextInt(); // Eingabe von dem auszuf�hrenden Progamm
		switch (programm) {
		case 1:
			aufgabe1();
			break;
		case 2:
			aufgabe2();
			break;
		case 3:
			aufgabe3();
			break;
		case 4:
			aufgabe4();
			break;
		default:
			System.out.println("Fehler!");
			break;
		}
	}

	// Aufgabe 1
	public static void aufgabe1() {
		// Deklaration
		int[] zahlenfeld = new int[10];

		// Array f�llen
		for (int i = 0; i < zahlenfeld.length; i++) {
			zahlenfeld[i] = i;
		}

		// Array ausgeben
		for (int i = 0; i < zahlenfeld.length; i++) {
			System.out.println(zahlenfeld[i]);
		}
	}

	// Aufgabe 2
	public static void aufgabe2() {
		// Deklaration
		int[] zahlenfeld = new int[10];

		// Array f�llen
		int x = 1;
		for (int i = 0; i < zahlenfeld.length; i++) {
			zahlenfeld[i] = x;
			x = x + 2;
		}

		// Array ausgeben
		for (int i = 0; i < zahlenfeld.length; i++) {
			System.out.println(zahlenfeld[i]);
		}
	}

	// Aufgabe 3
	public static void aufgabe3() {
		System.out.println("Bitte gebe ein Palindrom ein: ");
		String s = myScanner.next();
		char[] palindrom = s.toCharArray();
		for (int i = palindrom.length - 1; i >= 0; i--) {
			System.out.print(palindrom[i]);
		}
	}

	// Aufgabe 4
	public static void aufgabe4() {
		System.out.println("Diese Aufgabe habe ich nichtmehr gemacht, ich bin zum n�chsten AB gegeangen.");
	}
}
