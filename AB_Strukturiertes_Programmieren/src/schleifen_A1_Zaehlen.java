import java.util.Scanner;

public class schleifen_A1_Zaehlen {

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		int number;
		System.out.print("Soll ich hoch- oder runterz�hlen? (h/r)");
		char count = console.next().charAt(0);
		if (count == 'h') {
			System.out.print("Bis zur welcher Zahl soll ich z�hlen?");
			number = console.nextInt();
			System.out.println("\nOkay :)\n");
			for (int i = 0; i <= number; i++) {
				System.out.println(i);
			}
			System.out.println("\nDas hat Spa� gemacht :D");
		} else if (count == 'r') {
			System.out.print("Von welcher Zahl soll ich runterz�hlen?");
			number = console.nextInt();
			System.out.println("\nOkay :)\n");
			for (int i = number; i >= 0; i--) {
				System.out.println(i);
			}
			System.out.println("\nDas hat Spa� gemacht :D");
		} else {
			System.out.println("\nSie haben mich getrollt :D");
		}
	}

}
