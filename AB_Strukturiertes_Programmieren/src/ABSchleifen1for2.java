import java.util.Scanner;

//Pascal Smarslik
public class ABSchleifen1for2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte gebe die Seitenlänge von dem gewünschten Quadrat ein: ");
		int laenge = myScanner.nextInt();
		if (laenge > 0)
			quadratAusgeben(laenge);
	}

	private static void quadratAusgeben(int laenge) {
		for (int i = 1; i <= laenge; i++) {
			if (i == 1 || i == laenge) {
				for (int x = 1; x <= laenge; x++) {
					System.out.print("* ");
				}
			} else {
				for (int x = 1; x <= laenge; x++) {
					if (x == 1 || x == laenge) {
						System.out.print("* ");
					} else {
						System.out.print("  ");
					}
				}
			}
			System.out.println();
		}
	}

}
