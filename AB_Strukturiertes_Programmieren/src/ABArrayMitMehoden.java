import java.util.Scanner;

//Pascal Smarslik
public class ABArrayMitMehoden {
	static Scanner myScanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Welche Aufgabe soll ich ausführen? ( 1,2,3,4,5 oder 6 eingeben) ");
		int programm = myScanner.nextInt(); // Eingabe von dem auszuführenden Progamm
		switch (programm) {
		case 1:
			aufgabe1();
			break;
		case 2:
			aufgabe2();
			break;
		case 3:
			aufgabe3();
			break;
		case 4:
			aufgabe4();
			break;
		case 5:
			aufgabe5();
			break;
		case 6:
			aufgabe6();
			break;
		default:
			System.out.println("Fehler!");
			break;
		}
	}

	// Aufgabe 1
	public static void aufgabe1() {
		//System.out.println("Aufgabe 1 ist in Arbeit :)");
		int []zahlen= {1,3,4,5,6,8,9};
		String s=convertArrayToString(zahlen);
		System.out.print(s);
	}
	public static String convertArrayToString(int[]zahlen) {
		String s="";
		for (int i = 0; i<zahlen.length; i++) {
			s=s+zahlen[i];
			if(i<zahlen.length-1)
				s=s+",";
		}
		return s;
	}

	// Aufgabe 2
	public static void aufgabe2() {
		//System.out.println("Aufgabe 2 ist in Arbeit :)");		
		int [] zahlen = {1,2,3,4,5,6,7,8,9,10,11};
		zahlen=arrayUmdrehen(zahlen);
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}
	public static int[] arrayUmdrehen (int[]zahlen) {
		int parken;
		for(int i=0; i<zahlen.length/2; i++) {
			parken=zahlen[i];
			zahlen[i]=zahlen[zahlen.length-1-i];
			zahlen[zahlen.length-1-i]=parken;
		}
		return zahlen;
	}

	// Aufgabe 3
	public static void aufgabe3() {
		//System.out.println("Aufgabe 3 ist in Arbeit :)");
		int [] zahlen = {1,2,3,4,5,6,7,8,9,10,11};
		zahlen=arrayUmdrehenMitArray(zahlen);
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
	}
	public static int[] arrayUmdrehenMitArray (int[]zahlen) {
		int temp;
		for(int i=0; i<zahlen.length/2; i++) {
			temp=zahlen[i];
			zahlen[i]=zahlen[zahlen.length-1-i];
			zahlen[zahlen.length-1-i]=temp;
		}
		return zahlen;
	}

	// Aufgabe 4
	public static void aufgabe4() {
		System.out.println("Aufgabe 4 ist in Arbeit :)");
	}

	// Aufgabe 5
	public static void aufgabe5() {
		System.out.println("Aufgabe 5 ist in Arbeit :)");
	}

	// Aufgabe 6
	public static void aufgabe6() {
		System.out.println("Aufgabe 6 ist in Arbeit :)");
	}

}
