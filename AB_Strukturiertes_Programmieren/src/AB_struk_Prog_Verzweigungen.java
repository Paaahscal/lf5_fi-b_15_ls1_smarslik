
//Hier befase ich mich mit der Aufgabe 5, dem BMI
import java.util.Scanner;

public class AB_struk_Prog_Verzweigungen {

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		double bmi;
		String bmiType = "Klasse";
		System.out.println("Bitte geben Sie ihr Gewicht in kg ein:");
		double weight = console.nextFloat();
		System.out.println("Bitte geben Sie ihre Gr��e in cm ein:");
		double height = console.nextInt();
		System.out.println("Bitte geben Sie ihr Geschlecht an (m/w):");
		char sex = console.next().charAt(0);

		height = height / 100;
		bmi = weight / Math.pow(height, 2);

		if (bmi < 20 && sex == 'm' || bmi < 19 && sex == 'w')
			bmiType = "Untergewicht";
		if (20 <= bmi && bmi <= 25 && sex == 'm' || 19 <= bmi && bmi <= 24 && sex == 'w')
			bmiType = "Normalgewicht";
		if (bmi > 25 && sex == 'm' || bmi > 24 && sex == 'w')
			bmiType = "�bergewicht";

		System.out.printf("Sie haben einen BMI von %.1f und damit ein %s.", bmi, bmiType);
	}

}
