
public class Aufgabe3 {

	public static void main(String[] args) {//Pascal Smarslik
		int f1=-20; int f2=-10; int f3=0; int f4=20; int f5=30; //Ich lege alle Fahrenheitwerte fest, um sp�ter in der Formatausgabe einen besseren �berblick zu haben.
		double c1=-28.8889;//Ich lege alle Celsiuswerte fest, diese untereinander, da sie nicht so kurz wie die Fahrenheitswerte sind und somit der Code leichter zu lesen ist.
		double c2=-23.3333;
		double c3=-17.7778;
		double c4=-6.6667;
		double c5=-1.1111;
		String line="-----------------------------------------------";//Um auch die Zeile als Format und nicht abgez�hlt auszugeben, ist der String deutlich l�nger als hier ben�tigt.
		System.out.printf("%-12s|%10s\n", "Fahrenheit","Celsius"); //Ich gebe die oberste Zeile aus.
		System.out.printf("%.23s\n",line);// Die Linie zur �bersicht wird formatiert und ausgegeben.
		System.out.printf("%+-12d|%10.2f\n",f1,c1);//es werden alle Werte ausgegeben. Diese werden den Anforderungen entsprechend formartiert (links-, rechtsb�ndig, auf 2 Kommastellen und mit Vorzeichen.
		System.out.printf("%+-12d|%10.2f\n",f2,c2);//Die Kommentare habe ich einfach in einer Zeile gelassen, da diese etwas l�nger sind, oder soll ich diese k�rzen oder in mehreren Zeilen kommentieren?
		System.out.printf("%+-12d|%10.2f\n",f3,c3);
		System.out.printf("%+-12d|%10.2f\n",f4,c4);
		System.out.printf("%+-12d|%10.2f\n",f5,c5);
	}

}
